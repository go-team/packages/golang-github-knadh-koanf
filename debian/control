Source: golang-github-knadh-koanf
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-mitchellh-copystructure-dev,
               golang-github-mitchellh-mapstructure-dev,
               golang-github-fsnotify-fsnotify-dev,
               golang-github-joho-godotenv-dev,
               golang-github-hashicorp-hcl-dev,
               golang-github-hjson-hjson-go-dev,
               golang-github-pelletier-go-toml-dev,
               golang-github-aws-aws-sdk-go-v2-dev,
               golang-github-nats-io-go-nats-dev,
               golang-github-spf13-pflag-dev,
               golang-github-rhnvrm-simples3-dev,
               golang-github-fatih-structs-dev,
               golang-gopkg-yaml.v3-dev,
               golang-github-nats-io-nats-server-dev <!nocheck>,
               golang-github-stretchr-testify-dev <!nocheck>,
Standards-Version: 4.6.2
Homepage: https://github.com/knadh/koanf
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-knadh-koanf
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-knadh-koanf.git
XS-Go-Import-Path: github.com/knadh/koanf
Testsuite: autopkgtest-pkg-go

Package: golang-github-knadh-koanf-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-mitchellh-copystructure-dev,
         golang-github-mitchellh-mapstructure-dev,
         golang-github-fsnotify-fsnotify-dev,
         golang-github-joho-godotenv-dev,
         golang-github-hashicorp-hcl-dev,
         golang-github-hjson-hjson-go-dev,
         golang-github-pelletier-go-toml-dev,
         golang-github-aws-aws-sdk-go-v2-dev,
         golang-github-nats-io-go-nats-dev,
         golang-github-spf13-pflag-dev,
         golang-github-rhnvrm-simples3-dev,
         golang-github-fatih-structs-dev,
         golang-gopkg-yaml.v3-dev,
Description: extensible library for reading config (file, S3 etc.) in Go applications
 This package, koanf (pronounced conf; a play on the Japanese Koan) is
 a library for reading configuration from different sources in different
 formats in Go applications. It is a cleaner, lighter alternative to
 spf13/viper with better abstractions and extensibility and fewer
 dependencies.
 .
 koanf comes with built in support for reading configuration from files,
 command line flags, and environment variables, and can parse JSON, YAML,
 TOML, and Hashicorp HCL.
